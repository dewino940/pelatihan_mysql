Jawaban Nomor 1

mysql -uroot
create database myshop;
show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| myshop             |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
6 rows in set (0.001 sec)

Jawaban Nomor 2

create table users(
    -> id int(10) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );
create table categories(
    -> id int(10) primary key auto_increment,
    -> name varchar(255)
    -> );
create table items(
    -> id int(5) primary key auto_increment,
    -> name varchar(255),
    -> descriptions varchar(255),
    -> price int(8),
    -> stock int(3),
    -> category_id int(8),
    -> foreign key(category_id) references categories(id)
    -> );

Jawaban Nomor 3
insert into users(name, email, password)
    -> values("Jhon Doe","john@doe.com", "john123"),("Jane Doe", "jane@doe.com", "jenita123");
Query OK, 2 rows affected (0.003 sec)
Records: 2  Duplicates: 0  Warnings: 0

insert into categories(name)
    -> values("gadget"), ("cloth"), ("men"), ("women"), ("branded");
Query OK, 5 rows affected (0.005 sec)
Records: 5  Duplicates: 0  Warnings: 0

insert into items(name, descriptions, price, stock, category_id)
    -> values("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brnad ternama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);
Query OK, 3 rows affected (0.006 sec)
Records: 3  Duplicates: 0  Warnings: 0

Jawaban Nomor 4

a. 
select id, name, email from users;
+----+----------+--------------+
| id | name     | email        |
+----+----------+--------------+
|  1 | Jhon Doe | john@doe.com |
|  2 | Jane Doe | jane@doe.com |
+----+----------+--------------+
2 rows in set (0.001 sec)

b. 
-  select * from items where price > 1000000;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | descriptions                      | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
2 rows in set (0.001 sec)
-  select * from items where name like '%atc%';
+----+------------+-----------------------------------+---------+-------+-------------+
| id | name       | descriptions                      | price   | stock | category_id |
+----+------------+-----------------------------------+---------+-------+-------------+
|  3 | IMHO Watch | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+------------+-----------------------------------+---------+-------+-------------+
1 row in set (0.000 sec)

c. 
select items.id, items.name, items.descriptions, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.category_id = categories.id;
+----+-------------+-----------------------------------+---------+-------+-------------+--------+
| id | name        | descriptions                      | price   | stock | category_id | name   |
+----+-------------+-----------------------------------+---------+-------+-------------+--------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 | gadget |
|  2 | Uniklooh    | baju keren dari brnad ternama     |  500000 |    50 |           2 | cloth  |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget |
+----+-------------+-----------------------------------+---------+-------+-------------+--------+
3 rows in set (0.002 sec)

5. 
update items set price = 2500000 where id = 1;
Query OK, 1 row affected (0.003 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> select * from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | descriptions                      | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 2500000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brnad ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.000 sec)

